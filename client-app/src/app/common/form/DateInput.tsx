import React from "react";
import { FieldRenderProps } from "react-final-form";
import { FormFieldProps, Form, Label } from "semantic-ui-react";
import { DateTimePicker } from "react-widgets";

interface IProps extends FieldRenderProps<Date, HTMLInputElement>, FormFieldProps {}

const DateInput: React.FC<IProps> = ({ id, date = false, time = false, input, width, placeholder, meta: { touched, error }, ...rest }) => {
  return (
    <Form.Field error={touched && !!error} width={width}>
      <DateTimePicker 
        containerClassName="datepicker-container"
        date={date}
        time={time}
        id={id as string} 
        onChange={input.onChange} 
        onBlur={input.onBlur}
        onKeyDown={(e) => e.preventDefault()}
        placeholder={placeholder}
        {...rest}
        value={input.value || null}
        />
      {touched && error && (
        <Label basic color="red">
          {error}
        </Label>
      )}
    </Form.Field>
  );
};

export default DateInput;
